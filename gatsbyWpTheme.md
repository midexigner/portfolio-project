- npm i -g gatsby-cli
- gatsby --version
- gatsby new 
- gatsby develop
- https://www.npmjs.com/package/gatsby-wordpress-theme-phoenix
- open `gatsby-config.js` and delete the all content then add below code first install the  `gatsby-wordpress-theme-phoenix` this package
```sh
module.exports = {
    plugins: [
        // Tell gatsby which theme you will be using.
        {
        resolve: "gatsby-wordpress-theme-phoenix",
        options: {
            wordPressUrl: 'https://midexigner.com/',
            # frontendUrl: 'https://example.com'
        }
    } ]
};
```
